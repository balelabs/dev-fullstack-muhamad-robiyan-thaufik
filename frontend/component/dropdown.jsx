import { Menu } from "antd";

const { Item } = Menu;

const CDropdown = ({ dropdown }) => {
  return (
    <Menu style={{ border: "2px , solid, black" }}>
      {dropdown.map((action) => {
        return (
          <Item onClick={action.action} key={action.name}>
            {action.icon}
            {action.name}
          </Item>
        );
      })}
    </Menu>
  );
};

export default CDropdown;
