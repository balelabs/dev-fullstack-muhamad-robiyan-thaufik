import { Form, Input } from "antd";

const CInput = ({
  label,
  name,
  value,
  id,
  onChange,
  placeholder,
  addonBefore,
  require,
  disabled,
  validate,
  ...rest
}) => {
  return (
    <>
      <Form.Item
        label={
          <span>
            {label}
            {require ? (
              <span style={{ marginLeft: "5px", color: "red" }}>*</span>
            ) : null}
          </span>
        }
        name={name}
        {...rest}
        validateStatus={validate ? "error" : null}
        help={validate}
      >
        <Input
          addonBefore={addonBefore}
          value={value}
          id={id}
          disabled={disabled}
          placeholder={placeholder}
          onChange={onChange}
        />
      </Form.Item>
    </>
  );
};

export default CInput;
