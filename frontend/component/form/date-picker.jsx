import { DatePicker, Form } from "antd";
import moment from "moment";

const CDatePicker = ({
  value,
  onChange,
  placeholder,
  label,
  validate,
  ...rest
}) => {
  const dateFormat = "YYYY-MM-DD";
  const dates = value ? moment(value, dateFormat) : "";
  return (
    <Form.Item
      label={label}
      validateStatus={validate ? "error" : null}
      help={validate}
      {...rest}
    >
      <DatePicker
        onChange={onChange}
        showTime
        defaultValue={dates}
        format={dateFormat}
        placeholder={placeholder}
        value={dates}
        autoClose
      />
    </Form.Item>
  );
};

export default CDatePicker;
