import { Form, Select } from "antd";

const { Option } = Select;

const CSelectOption = ({
  label,
  name,
  defaultValue,
  placeholder,
  onChange,
  data,
  size,
  disabled,
  require,
  validate,
  value,
  ...rest
}) => {
  return (
    <>
      <Form.Item
        label={label}
        name={name}
        {...rest}
        validateStatus={validate ? "error" : null}
        help={validate}
      >
        <Select
          placeholder={placeholder}
          showSearch
          disabled={disabled ? true : false}
          {...rest}
          style={{ minHeight: "100%" }}
          size={size}
          value={value}
          optionFilterProp="children"
          onChange={onChange}
          filterOption={(input, option) =>
            option.props.children.toLowerCase().indexOf(input.toLowerCase()) >=
            0
          }
        >
          {data.map(function (item) {
            return (
              <Option key={item.id} value={item.id}>
                {item.name}
              </Option>
            );
          })}
        </Select>
      </Form.Item>
    </>
  );
};

export default CSelectOption;
