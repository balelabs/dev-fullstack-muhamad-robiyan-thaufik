import {
  MenuFoldOutlined,
  MenuUnfoldOutlined,
  UserOutlined,
} from "@ant-design/icons";
import { Avatar, Dropdown, Menu, Layout, Typography } from "antd";
import { useRouter } from "next/router";
import { createElement } from "react";

const { Text } = Typography;
const { Header } = Layout;
const TheHeader = ({ collapsed, toggle }) => {
  const router = useRouter();
  const homepage = (e) => {
    e.preventDefault();
    router.push("/");
    document.body.style.overflow = "auto";
  };
  const menu = (
    <Menu
      items={[
        {
          label: (
            <div>
              <UserOutlined /> Profile
            </div>
          ),
          key: "profile",
        },
      ]}
    />
  );
  return (
    <div>
      <Header
        style={{ position: "fixed", zIndex: 1, width: "100%" }}
        className="header"
      >
        {createElement(collapsed ? MenuUnfoldOutlined : MenuFoldOutlined, {
          className: "trigger",
          onClick: () => toggle(),
        })}
        <a href="/" onClick={homepage}>
          <div className="logo" />
          <span style={{ color: "#ffffff" }}> - </span>
        </a>
        <Dropdown className="c-head" overlay={menu} trigger={["click"]}>
          <a
            href="/"
            className="ant-dropdown-link"
            onClick={(e) => e.preventDefault()}
          >
            <Avatar size={40} icon={<UserOutlined />} />
            <span style={{ color: "#ffffff" }}> - </span>
          </a>
        </Dropdown>
        <Text
          style={{ marginRight: 15, color: "#d9363e" }}
          className="c-head"
          strong
        >
          Admin
        </Text>
      </Header>
    </div>
  );
};

export default TheHeader;
