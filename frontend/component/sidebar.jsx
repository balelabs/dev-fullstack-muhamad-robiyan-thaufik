import {
  DropboxOutlined,
  FileTextOutlined,
  UsergroupDeleteOutlined,
} from "@ant-design/icons";
import { Layout, Menu } from "antd";
import { useRouter } from "next/router";

const { Sider } = Layout;
const Sidebar = ({ collapsed }) => {
  const router = useRouter();
  function getItem(label, key, icon, children) {
    return {
      key,
      icon,
      children,
      label,
    };
  }

  const items = [
    getItem(
      <a
        href="/"
        onClick={(e) => {
          e.preventDefault();
          router.push("/");
        }}
      >
        Customer
      </a>,
      "/",
      <UsergroupDeleteOutlined />
    ),
    getItem(
      <a
        href="/package"
        onClick={(e) => {
          e.preventDefault();
          router.push("/package");
        }}
      >
        Package
      </a>,
      "/package",
      <DropboxOutlined />
    ),
    // getItem(
    //   <a
    //     href="/invoice"
    //     onClick={(e) => {
    //       e.preventDefault();
    //       router.push("/invoice");
    //     }}
    //   >
    //     Invoice
    //   </a>,
    //   "/invoice",
    //   <FileTextOutlined />
    // ),
    getItem(
      <a
        href="/invoice-line"
        onClick={(e) => {
          e.preventDefault();
          router.push("/invoice-line");
        }}
      >
        Invoice line
      </a>,
      "/invoice-line",
      <FileTextOutlined />
    ),
  ];

  return (
    <Sider
      className="sider-c"
      width={220}
      trigger={null}
      collapsible
      collapsed={collapsed}
    >
      <Menu theme="dark" className="my-menu" mode="inline" items={items} />
    </Sider>
  );
};
export default Sidebar;
