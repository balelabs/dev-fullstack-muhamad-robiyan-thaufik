import axios from "axios";

export const MyApi = axios.create({
  baseURL: process.env.NEXT_PUBLIC_BASE_URL || "http://localhost:8000/api/",
  headers: {
    "X-Requested-With": "XMLHttpRequest",
    "X-Custom-Header": "foobar",
    "Content-Type": "application/json",
    Accept: "application/json",
    responseType: "json",
  },
});
