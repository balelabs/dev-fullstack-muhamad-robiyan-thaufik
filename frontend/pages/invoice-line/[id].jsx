import { Button, Card, Col, notification, Row, Typography } from "antd";
import ListWhite from "../../component/list-white";
import ListGrey from "../../component/list-grey";
import CTitle from "../../component/title";
import { useEffect, useState } from "react";
import { MyApi } from "../api/hello";
import { useRouter } from "next/router";
import Loading from "../../component/loading";

const { Title } = Typography;
const InvoiceLineDetail = () => {
  const [isLoading, setIsLoading] = useState(true);
  const router = useRouter();
  useEffect(() => {
    getData();
  }, []);

  const errorNotification = (msg) => {
    notification.error({
      message: "Error",
      description: msg,
    });
  };
  const getData = () => {
    setIsLoading(true);
    MyApi({
      url: "invoice-line/" + router.query.id,
      method: "get",
    })
      .then((res) => {
        setDetail(res.data[0]);
      })
      .catch((err) => {
        errorNotification(err.response.data.message);
      })
      .finally(() => {
        setIsLoading(false);
      });
  };
  const [detail, setDetail] = useState({
    address: "",
    amount: 0,
    customer_id: 0,
    email: "",
    inv_date: "",
    inv_number: "",
    name: "",
    packagesDesc: "",
    packagesName: "",
    packages_id: 0,
    phone: "",
    sales_invoice_id: 0,
    total_amount: 0,
    total_discount_amount: 0,
  });

  if (isLoading) return <Loading />;

  const renderRp = (value) => {
    return "Rp. " + value.toFixed(0).replace(/(\d)(?=(\d{3})+(?!\d))/g, "$1.");
  };

  return (
    <Row>
      <CTitle title={"Detail Invoice " + detail.inv_number} />
      <Col
        span={24}
        style={{
          backgroundColor: "#1890ff",
          paddingTop: "2px",
          marginBottom: 75,
        }}
      >
        <Card>
          <Title level={2}>Detail Invoice {detail.inv_number}</Title>
          <Row gutter={4} style={{ flex: 1 }}>
            <Col span={24} style={{ marginTop: 10 }}>
              <Title level={4}>Customer</Title>
              <ListGrey label="Name" value={detail.name} />
              <ListWhite label="Email" value={detail.email} />
              <ListGrey label="Phone" value={detail.phone} />
              <ListWhite label="Address" value={detail.address} />
              <hr />
            </Col>
            <Col span={24} style={{ marginTop: 10 }}>
              <Title level={4}>Package</Title>
              <ListGrey label="Name" value={detail.packagesName} />
              <ListWhite label="Description" value={detail.packagesDesc} />
              <hr />
            </Col>
            <Col span={24} style={{ marginTop: 10 }}>
              <Title level={4}>Invoice</Title>
              <ListGrey label="Costumer Name" value={detail.name} />
              <ListWhite label="Invoice Number" value={detail.inv_number} />
              <ListGrey label="Invoice Date" value={detail.inv_date} />
              <hr />
            </Col>
            <Col span={24} style={{ marginTop: 10 }}>
              <Title level={4}>Invoice Line</Title>
              <ListGrey label="Invoice Number" value={detail.inv_number} />
              <ListWhite label="Package Name" value={detail.packagesName} />
              <ListGrey label="Amount" value={renderRp(detail.amount)} />
              <ListWhite
                label="Discount Amount"
                value={renderRp(detail.total_discount_amount)}
              />
              <ListGrey
                label="Total Amount"
                value={renderRp(detail.total_amount)}
              />
              <hr />
            </Col>
          </Row>
          <Row justify="center">
            <Button
              type="primary"
              onClick={() => router.back()}
              style={{ color: "#ffffff" }}
            >
              Back
            </Button>
          </Row>
        </Card>
      </Col>
    </Row>
  );
};

export default InvoiceLineDetail;
