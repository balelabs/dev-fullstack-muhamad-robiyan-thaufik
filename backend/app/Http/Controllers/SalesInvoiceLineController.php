<?php

namespace App\Http\Controllers;

use App\Http\Requests\SalesInvoiceRequest;
use App\Models\Package;
use App\Models\SalesInvoiceLine;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class SalesInvoiceLineController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $data = DB::table('sales_invoice_lines')
        ->select(
            'sales_invoice_lines.id',
            'packages.id as package_id',
            'sales_invoices.id as sales_invoice_id',
            'customers.name',
            'sales_invoices.customer_id',
            'sales_invoices.inv_number',
            'sales_invoices.inv_date',
            'sales_invoices.amount',
            'sales_invoices.total_discount_amount',
            'sales_invoices.total_amount',
            'sales_invoice_lines.id as invoice_line_id'
        )
        ->join('sales_invoices', 'sales_invoice_lines.sales_invoice_id', '=', 'sales_invoices.id')
        ->join('customers', 'sales_invoices.customer_id', '=', 'customers.id')
        ->join('packages', 'sales_invoice_lines.package_id', '=', 'packages.id')
        ->orderBy('sales_invoice_lines.updated_at', 'DESC')
        ->paginate($request->per_page);

        return response()->json($data);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(SalesInvoiceRequest $request)
    {
        $package = Package::find($request->package_id);

        $invId = DB::table('sales_invoices')->insertGetId([
            'customer_id' => $request->customer_id,
            'inv_number' => $request->inv_number,
            'inv_date' => $request->inv_date,
            'amount' => $package->amount,
            'total_discount_amount' => $request->total_discount_amount,
            'total_amount' => $package->amount - $request->total_discount_amount,
            'created_at'=> self::timestamp(),
            'updated_at'=> self::timestamp(),
        ]);

        DB::table('sales_invoice_lines')->insert([
            'amount' => $package->amount,
            'discount_amount' => $request->total_discount_amount,
            'total_amount' => $package->amount - $request->total_discount_amount,
            'sales_invoice_id' => $invId,
            'package_id' => $package->id,
            'created_at'=> self::timestamp(),
            'updated_at'=> self::timestamp(),
        ]);

        return response()->json([
            'message' => 'Data has been saved...'
        ]);
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        $data =DB::table('sales_invoice_lines')
        ->select(
            'sales_invoice_lines.id',
            // Packages
            'packages.id as packages_id',
            'packages.amount',
            'packages.name as packagesName',
            'packages.description as packagesDesc',

            // Customer
            'customers.name',
            'customers.email',
            'customers.phone',
            'customers.address',

            // sales_invoices
            'sales_invoices.id as sales_invoice_id',
            'sales_invoices.customer_id',
            'sales_invoices.inv_number',
            'sales_invoices.inv_date',
            'sales_invoices.total_discount_amount',
            'sales_invoices.total_amount',

            // sales_invoice_lines
            'sales_invoice_lines.package_id'
        )
        ->where('sales_invoice_lines.id', $id)
        ->join('sales_invoices', 'sales_invoice_lines.sales_invoice_id', '=', 'sales_invoices.id')
        ->join('customers', 'sales_invoices.customer_id', '=', 'customers.id')
        ->join('packages', 'sales_invoice_lines.package_id', '=', 'packages.id')
        ->get();

        return response()->json($data);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(SalesInvoiceRequest $request, $id)
    {
        $package = Package::find($request->package_id);

        DB::table('sales_invoices')
        ->where('id', $request->sales_invoice_id)
        ->update([
            'customer_id' => $request->customer_id,
            'inv_number' => $request->inv_number,
            'inv_date' => $request->inv_date,
            'amount' => $package->amount,
            'total_discount_amount' => $request->total_discount_amount,
            'total_amount' => $package->amount - $request->total_discount_amount,
            'updated_at'=> self::timestamp(),
        ]);

        DB::table('sales_invoice_lines')
        ->where('id', $id)
        ->update([
            'amount' => $package->amount,
            'discount_amount' => $request->total_discount_amount,
            'total_amount' => $package->amount - $request->total_discount_amount,
            'package_id' => $package->id,
            'updated_at'=> self::timestamp(),
        ]);

        return response()->json([
            'message' => 'Data has been updated...'
        ]);
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $invoiceID = SalesInvoiceLine::find($id);
        DB::table('sales_invoices')->where('id', $invoiceID->sales_invoice_id)->delete();
        $invoiceID->delete();

        return response()->json([
            'message' => 'Data has been deleted...',
        ]);
    }

    static function timestamp(){
        $unixEpoch = round(microtime(true) * 1000);
        // $date = Carbon::createFromTimestampMs($unixEpoch, 'Asia/Jakarta')->format('Ymd H:i:s');

        return $unixEpoch;
    }
}
