<?php

use App\Http\Controllers\CustomerController;
use App\Http\Controllers\PackageController;
use App\Http\Controllers\SalesInvoiceController;
use App\Http\Controllers\SalesInvoiceLineController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

// Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
//     return $request->user();
// });

Route::get('customer', [CustomerController::class, 'index']);
Route::get('package', [PackageController::class, 'index']);
Route::get('package/{id}', [PackageController::class, 'show']);
Route::get('package-list', [PackageController::class, 'getList']);
Route::get('customer-list', [CustomerController::class, 'getList']);


Route::get('invoice', [CustomerController::class, 'index']);

Route::resource('invoice-line', SalesInvoiceLineController::class)->except(['create','edit']);
