<?php

namespace Database\Seeders;

use App\Models\Package;
use Illuminate\Database\Seeder;

class PackageSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
            $data = [
                [
                    'name' => "Premium",
                    'description' => "Full Package",
                    'amount' => 1000000,
                ],
                [
                    'name' => "Member",
                    'description' => "Common",
                    'amount' => 450000,
                ],
            ];
        Package::insert($data);
    }
}
