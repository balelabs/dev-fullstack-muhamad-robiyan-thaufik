<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateSalesInvoiceLinesTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('sales_invoice_lines', function (Blueprint $table) {
            $table->id();
            $table->bigInteger('amount');
            $table->bigInteger('discount_amount');
            $table->bigInteger('total_amount');
            $table->unsignedBigInteger('sales_invoice_id');
            $table->unsignedBigInteger('package_id');
            $table->bigInteger('created_at');
            $table->bigInteger('updated_at');
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('sales_invoice_lines');
    }
}
